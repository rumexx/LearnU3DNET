﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float horizontalSpeed = 100.0f;
    public float verticalSpeed = 80.0f;

    private Transform playHandle;
    private Transform cameraHandle;
    private PlayerInput pi;
    private float tempEulerX;

    private void Awake() {
        cameraHandle = transform.parent;
        playHandle = cameraHandle.transform.parent;

        pi = playHandle.gameObject.GetComponent<PlayerInput>();
    }

    // Update is called once per frame
    void Update()
    {
        // playHandle.Rotate(Vector3.up, 10.0f * Time.deltaTime);

        // tempEulerX -= -verticalSpeed * Time.deltaTime;
        // cameraHandle.transform.eulerAngles = new Vector3(Mathf.Clamp(tempEulerX, -40, 30), 0, 0);
    }
}
