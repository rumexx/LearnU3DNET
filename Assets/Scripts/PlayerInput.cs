﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{

    private float dirForward;
    private float dirRight;
    private float velForward;
    private float velRight;
    public float curForward;
    public float curRight;

    public float Dmag;
    public Vector3 DVec;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        dirForward = (Input.GetKey(KeyCode.W)? 1.0f:0) - (Input.GetKey(KeyCode.S)? 1.0f:0);
        dirRight = (Input.GetKey(KeyCode.D)? 1.0f:0) - (Input.GetKey(KeyCode.A)? 1.0f:0);

        curForward = Mathf.SmoothDamp(curForward, dirForward, ref velForward, 0.01f);
        curRight = Mathf.SmoothDamp(curRight, dirRight, ref velRight, 0.01f);

        Vector2 norVec = new Vector2(curForward, curRight).normalized;

        Dmag = Mathf.Sqrt(norVec.x * norVec.x + norVec.y * norVec.y);
        DVec = transform.forward * norVec.x + transform.right * norVec.y; 

  
    }
}
