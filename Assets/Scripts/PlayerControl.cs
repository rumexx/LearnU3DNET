﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float moveSpeed = 2.0f;
    private PlayerInput pi;
    private Rigidbody rigid;   
    private GameObject model;  
    private Vector3 moveVec;
 

 
    void Awake()
    {
        pi = GetComponent<PlayerInput>();
        rigid = GetComponent<Rigidbody>();
        
        model = GameObject.Find("Model");
    }

    // Update is called once per frame
    void Update()
    {
        if(pi.Dmag > 0.1f)
        {
            model.transform.forward = Vector3.Slerp(model.transform.forward, pi.DVec, 0.2f);
        }
        
        moveVec = model.transform.forward * pi.Dmag * moveSpeed;
    }


    private void FixedUpdate() 
    {
        // rigid.position += moveVec * Time.fixedDeltaTime;    
        rigid.velocity = new Vector3(moveVec.x, rigid.velocity.y, moveVec.z);  ;
    }

}
